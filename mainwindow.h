#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QImage>
#include"libqrencode/qrencode.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void gen();

private:
      void initSignals();
      void initList();
     QString getInputText();
     QRencodeMode getEncodeMode();
     int getVersion();

     QRecLevel getErrLevel();
     bool getMqrFlag();
     int getMask();
     QImage genQRCode(QString text,QRencodeMode mode,int version,QRecLevel errLevel,int mask,bool mqr);
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
