#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<cstring>
#include<algorithm>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initList();
    initSignals();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::gen()
{

    QString inputStr=this->getInputText();
    QRencodeMode mode=this->getEncodeMode();
    int version=this->getVersion();
    int mask=this->getMask();
    QRecLevel  errLevel=this->getErrLevel();
    bool mqrFlag=this->getMqrFlag();
    QImage img=this->genQRCode(inputStr,mode,version,errLevel,mask,mqrFlag);

    int s=std::min(ui->label->width(),ui->label->height());
    ui->label->setPixmap(QPixmap::fromImage(img).scaled(s,s));
}

void MainWindow::initSignals()
{
    connect(ui->btn_gen,SIGNAL(clicked(bool)),this,SLOT(gen()));
}

void MainWindow::initList()
{
    for(int i=0;i<=40;i++){
        ui->versionCombo->addItem(QString("%1").arg(i));
    }
    for(int i=0;i<=7;i++){
        ui->maskCombo->addItem(QString("%1").arg(i));
    }
}

QString MainWindow::getInputText()
{
    return ui->inputEdit->text();
}

QRencodeMode MainWindow::getEncodeMode()
{

    int index=ui->comboBox->currentIndex();
    switch (index ){
    case 0:
        return QR_MODE_NUM;
    case 1:
        return QR_MODE_AN;
    case 2:
        return QR_MODE_8;
    case 3:
        return QR_MODE_KANJI;
    case 4:
        return QR_MODE_STRUCTURE;
    case 5:
        return QR_MODE_ECI;
    case 6:
        return QR_MODE_FNC1FIRST;
    case 7:
        return QR_MODE_FNC1SECOND;
    default:
        return QR_MODE_STRUCTURE;
    }
}

int MainWindow::getVersion()
{
    return ui->versionCombo->currentIndex();
}

QRecLevel MainWindow::getErrLevel()
{
    return QRecLevel(ui->ErrCombo->currentIndex());
}

bool MainWindow::getMqrFlag()
{
    return ui->mqrFlag->checkState()==Qt::Checked;
}

int MainWindow::getMask()
{
    return ui->maskCombo->currentIndex();
}

QImage MainWindow::genQRCode(QString text, QRencodeMode mode, int version, QRecLevel errLevel, int mask,bool mqr)
{
    QRinput *stream;
    QImage img;
    unsigned char *frame;
    int  w,ret;
    QByteArray arr=text.toUtf8();
    QRcode *qrcode;
    if (mqr){
        stream=QRinput_newMQR(version,errLevel);
    }else{
        stream=QRinput_new2(version,errLevel);
    }
    if(stream == NULL) goto OUT;
    QRinput_append(stream, mode,arr.length(), (unsigned char *)arr.data());
    QRinput_setErrorCorrectionLevel(stream, errLevel);
    if(ret < 0) {
            QRinput_free(stream);
            goto OUT;
    }
    qrcode = QRcode_encodeMask(stream, mask);
    if(qrcode == NULL) goto OUT;
    w = qrcode->width;
    frame = qrcode->data;
    img=QImage(w,w,QImage::Format_RGB888);
    for(int y=0;y<w;y++){
        for (int x=0;x<w;x++){
            if(frame[y*w+x]&0x1)
                img.setPixel(x,y,qRgb(0,0,0));
            else
                img.setPixel(x,y,qRgb(255,255,255));
        }
    }
    QRcode_free(qrcode);
    QRinput_free(stream);
OUT:
    return img;
}
